<?php

namespace Drupal\cloudfront_edge_caching;

use Aws\CloudFront\CloudFrontClient;
use Aws\Exception\AwsException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\key_aws\AWSKeyRepository;

/**
 * Cloudfront Edge Cache service class.
 */
class CloudfrontEdgeCache {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The AWS key repository object.
   *
   * @var \Drupal\key_aws\AWSKeyRepository
   */
  protected $awsKeyRepository;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\key_aws\AWSKeyRepository $aws_key_repository
   *   AWS Key repository object.
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    AWSKeyRepository $aws_key_repository
  ) {
    $this->logger = $logger_factory->get('cloudfront_edge_caching');
    $this->config = $config->getEditable('cloudfront_edge_caching.settings');
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->awsKeyRepository = $aws_key_repository;
  }

  /**
   * Get configuration.
   *
   * @return \Drupal\Core\Config\Config|null
   *   Returns the config.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Get client.
   *
   * @return \Aws\CloudFront\CloudFrontClient|null
   *   Return the client.
   */
  public function getClient(): ?CloudFrontClient {
    $region = $this->getConfig()->get('cec_region');
    if (empty($region)) {
      return NULL;
    }

    // Get credentials.
    $this->awsKeyRepository->setKey($this->getConfig()->get('cec_aws_key_name'));
    if (!$this->awsKeyRepository->getCredentials()) {
      return NULL;
    }

    return new CloudFrontClient([
      'version' => 'latest',
      'region' => $region,
      'credentials' => $this->awsKeyRepository->getClientCredentials(),
    ]);
  }

  /**
   * Get distributions.
   *
   * @return array|\Aws\Result
   *   Return the array of distributions.
   */
  public function getDistributions() {
    $distributions = [];
    if ($client = $this->getClient()) {
      try {
        // @todo Cache distributions.
        $distributions = $client->listDistributions([]);
      }
      catch (AwsException $e) {
        watchdog_exception('cloudfront_edge_caching', $e);
      }
    }
    return $distributions;
  }

  /**
   * Get invalidations.
   *
   * @return array
   *   Return the array of invalidations.
   */
  public function getInvalidations() {
    $invalidations = [];
    if ($client = $this->getClient()) {
      $distribution_id = $this->getConfig()->get('cec_distribution_id');
      try {
        // @todo Cache invalidations.
        $invalidations = $client->listInvalidations([
          'DistributionId' => $distribution_id,
        ]);
      }
      catch (AwsException $e) {
        watchdog_exception('cloudfront_edge_caching', $e);
      }
    }
    return $invalidations;
  }

  /**
   * Invalidate url/paths.
   *
   * @param array $paths
   *   Array of paths to invalidate.
   *
   * @return \Aws\Result|false
   *   Returns the result or false if failed.
   */
  public function invalidateUrl(array $paths) {
    $result = FALSE;
    if ($client = $this->getClient()) {
      $distribution_id = $this->getConfig()->get('cec_distribution_id');
      try {
        $result = $client->createInvalidation([
          'DistributionId' => $distribution_id,
          'InvalidationBatch' => [
            'CallerReference' => 'invalidation-' . time(),
            'Paths' => [
              'Items' => $paths,
              'Quantity' => count($paths),
            ],
          ],
        ]);
      }
      catch (AwsException $e) {
        watchdog_exception('cloudfront_edge_caching', $e);
      }
    }
    return $result;
  }

}
