<?php

namespace Drupal\cloudfront_edge_caching\Controller;

use Drupal\cloudfront_edge_caching\CloudfrontEdgeCache;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Cloudfront Edge Cache Controller.
 */
class CloudfrontEdgeCacheController extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The cloudfront edge cache service.
   *
   * @var \Drupal\cloudfront_edge_caching\CloudfrontEdgeCache
   */
  protected $cloudfrontEdgeCache;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\cloudfront_edge_caching\CloudfrontEdgeCache $cloudfront_edge_cache
   *   The cloudfront edge cache service.
   */
  public function __construct(RequestStack $request_stack, CloudfrontEdgeCache $cloudfront_edge_cache) {
    $this->requestStack = $request_stack;
    $this->cloudfrontEdgeCache = $cloudfront_edge_cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('cloudfront_edge_cache'),
    );
  }

  /**
   * Get distributions.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   Return table of distributions.
   */
  public function getDistributions(Request $request): array {
    $build = [
      '#type' => 'table',
      '#empty' => $this->t('No distributions available at this time.'),
    ];

    $build['#header'] = [
      'id' => $this->t('Distribution Id'),
      'status' => $this->t('Status'),
      'modified' => $this->t('Last Modified'),
    ];

    $rows = [];
    $distributions = $this->cloudfrontEdgeCache->getDistributions();
    if (isset($distributions['DistributionList'])) {
      if ($distributions['DistributionList']['Quantity'] > 0) {
        foreach ($distributions['DistributionList']['Items'] as $distribution) {
          /** @var \Aws\Api\DateTimeResult $created */
          $modified = $distribution['LastModifiedTime'];
          $rows[] = [
            'id' => $distribution['Id'],
            'status' => $distribution['Status'],
            'modified' => ($modified) ? $modified->format('Y-m-d H:i:s') : '',
          ];
        }
      }
    }
    $build['#rows'] = $rows;

    return $build;
  }

  /**
   * Get invalidations.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   Return table of invalidations.
   */
  public function getInvalidations(Request $request): array {
    $build = [
      '#type' => 'table',
      '#empty' => $this->t('No invalidations available at this time.'),
    ];

    $build['#header'] = [
      'id' => $this->t('Invalidation Id'),
      'status' => $this->t('Status'),
      'created' => $this->t('Created'),
    ];

    $rows = [];
    $invalidations = $this->cloudfrontEdgeCache->getInvalidations();
    if (isset($invalidations['InvalidationList'])) {
      if ($invalidations['InvalidationList']['Quantity'] > 0) {
        foreach ($invalidations['InvalidationList']['Items'] as $invalidation) {
          /** @var \Aws\Api\DateTimeResult $created */
          $created = $invalidation['CreateTime'];
          $rows[] = [
            'id' => $invalidation['Id'],
            'status' => $invalidation['Status'],
            'created' => ($created) ? $created->format('Y-m-d H:i:s') : '',
          ];
        }
      }
    }
    $build['#rows'] = $rows;

    return $build;
  }

  /**
   * Reload the previous page.
   *
   * @return mixed|string
   *   Returns the uri.
   */
  public function reloadPage() {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->server->get('HTTP_REFERER')) {
      return $request->server->get('HTTP_REFERER');
    }
    else {
      return '/';
    }
  }

  /**
   * Flush site cache in Cloudfront.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function flushCache(Request $request) {
    $result = $this->cloudfrontEdgeCache->invalidateUrl(['/']);
    if ($result) {
      $data = $result->toArray();
      $this->messenger()
        ->addStatus($this->t('Your site cache invalidation: @id, is in progress.', ['@id' => $data['Invalidation']['Id']]), 'status');
    }
    else {
      $this->messenger()->addError($this->t('Failed to invalidate site cache. Please try again or check log for further information.'), 'error');
    }

    return new RedirectResponse($this->reloadPage());
  }

}
