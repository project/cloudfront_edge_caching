<?php

namespace Drupal\cloudfront_edge_caching\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure settings for Cloudfront credentials.
 */
class CloudfrontEdgeCacheSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cec_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudfront_edge_caching.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cloudfront_edge_caching.settings');

    $key_collection_url = Url::fromRoute('entity.key.collection')->toString();
    $form['keys'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Keys'),
      '#description' => $this->t('Use keys managed by the key module. <a href=":keys">Manage keys</a>', [
        ':keys' => $key_collection_url,
      ]),
      '#tree' => FALSE,
    ];

    $form['keys']['cec_aws_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('AWS Credentials'),
      '#description' => $this->t('AWS credentials key used to store authentication keys.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => $config->get('cec_aws_key_name'),
      '#required' => TRUE,
      '#key_filters' => [
        'provider' => [
          'aws_config',
          'aws_file',
        ],
      ],
    ];

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    // Regions.
    $regions = [
      '' => $this->t('- Select Region -'),
      'af-south-1' => $this->t('af-south-1'),
      'ap-east-1' => $this->t('ap-east-1'),
      'ap-northeast-1' => $this->t('ap-northeast-1'),
      'ap-northeast-2' => $this->t('ap-northeast-2'),
      'ap-northeast-3' => $this->t('ap-northeast-3'),
      'ap-south-1' => $this->t('ap-south-1'),
      'ap-southeast-1' => $this->t('ap-southeast-1'),
      'ap-southeast-2' => $this->t('ap-southeast-2'),
      'ap-southeast-3' => $this->t('ap-southeast-3'),
      'ca-central-1' => $this->t('ca-central-1'),
      'cn-north-1' => $this->t('cn-north-1'),
      'cn-northwest-1' => $this->t('cn-northwest-1'),
      'eu-central-1' => $this->t('eu-central-1'),
      'eu-north-1' => $this->t('eu-north-1'),
      'eu-south-1' => $this->t('eu-south-1'),
      'eu-west-1' => $this->t('eu-west-1'),
      'eu-west-2' => $this->t('eu-west-2'),
      'eu-west-3' => $this->t('eu-west-3'),
      'me-south-1' => $this->t('me-south-1'),
      'sa-east-1' => $this->t('sa-east-1'),
      'us-east-1' => $this->t('us-east-1'),
      'us-east-2' => $this->t('us-east-2'),
      'us-gov-east-1' => $this->t('us-gov-east-1'),
      'us-gov-west-1' => $this->t('us-gov-west-1'),
      'us-west-1' => $this->t('us-west-1'),
      'us-west-2' => $this->t('us-west-2'),
    ];
    $form['settings']['cec_region'] = [
      '#type' => 'select',
      '#title' => $this->t('Region'),
      '#default_value' => $config->get('cec_region'),
      '#options' => $regions,
      '#description' => $this->t('Eg: us-east-1'),
      '#required' => TRUE,
    ];

    // Distribution ID.
    $form['settings']['cec_distribution_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Distribution ID'),
      '#default_value' => $config->get('cec_distribution_id'),
      '#size' => 20,
      '#maxlength' => 128,
      '#description' => $this->t('Eg: E206SWIPUZ2Z48'),
      '#required' => TRUE,
    ];

    $form['cache'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cache configuration'),
    ];

    $form['cache']['cec_auto_clear_cache'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Auto clear cache for entities'),
      '#options' => [
        'node' => $this->t('Node'),
        'user' => $this->t('User'),
      ],
    ];
    if (is_array($config->get('cec_auto_clear_cache'))) {
      $form['cache']['cec_auto_clear_cache']['#default_value'] = $config->get('cec_auto_clear_cache');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory()->getEditable('cloudfront_edge_caching.settings')
      ->set('cec_region', $form_state->getValue('cec_region'))
      ->set('cec_aws_key_name', $form_state->getValue('cec_aws_key_name'))
      ->set('cec_distribution_id', $form_state->getValue('cec_distribution_id'))
      ->set('cec_auto_clear_cache', $form_state->getValue('cec_auto_clear_cache'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
