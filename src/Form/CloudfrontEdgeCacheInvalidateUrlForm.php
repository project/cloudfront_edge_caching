<?php

namespace Drupal\cloudfront_edge_caching\Form;

use Drupal\cloudfront_edge_caching\CloudfrontEdgeCache;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Invalidate manual URL.
 */
class CloudfrontEdgeCacheInvalidateUrlForm extends FormBase {

  /**
   * The cloudfront edge cache service.
   *
   * @var \Drupal\cloudfront_edge_caching\CloudfrontEdgeCache
   */
  protected $cloudfrontEdgeCache;

  /**
   * Constructor.
   *
   * @param \Drupal\cloudfront_edge_caching\CloudfrontEdgeCache $cloudfront_edge_cache
   *   The cloudfront edge cache service.
   */
  public function __construct(CloudfrontEdgeCache $cloudfront_edge_cache) {
    $this->cloudfrontEdgeCache = $cloudfront_edge_cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cloudfront_edge_cache'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cec_admin_invalidate_url_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('URLs to invalidate'),
      '#description' => $this->t('Specify the existing path you wish to invalidate. For example: /node/28, /forum/1. Enter one value per line'),
      '#required' => TRUE,
    ];

    $form['invalidate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Invalidate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get the URL.
    $url_value = explode("\n", $form_state->getValue('urls'));
    if (!empty($url_value) && is_array($url_value) && count($url_value) > 0) {
      foreach ($url_value as $value) {
        if (substr($value, 0, 1) != '/' && !empty($value)) {
          $form_state->setErrorByName('urls', $this->t('The URL introduced is not valid.'));
        }
      }
    }
    else {
      $form_state->setErrorByName('urls', $this->t('The URL introduced is not valid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $urls = explode("\n", $form_state->getValue('urls'));

    // Check if the credentials are configured.
    if (!$this->cloudfrontEdgeCache->getClient()) {
      $this->messenger()
        ->addError($this->t('Failed to get AWS client. Please make sure settings are configured properly.'), 'error');
    }

    else {
      // Get the Paths.
      $paths = [];
      foreach ($urls as $url) {
        if ($url) {
          $paths[] = trim($url);
        }
      }

      // Invalidate.
      $result = $this->cloudfrontEdgeCache->invalidateUrl($paths);
      if ($result) {
        /** @var \Aws\Result $result */
        $data = $result->toArray();
        $this->messenger()
          ->addStatus($this->t('Your invalidation, @id is in progress.', ['@id' => $data['Invalidation']['Id']]), 'status');
      }
      else {
        $this->messenger()->addError($this->t('Failed to invalidate urls. Please try again or check logs.'), 'error');
      }
    }
  }

}
